
var gitKataBestCommiter = {
  
  /**
   * @type {string}
   * @private
   */  
   searchOnBitBucket_: 'https://api.bitbucket.org/1.0/repositories/wojtassj/git-kata-registration/changesets',
  
  /**
   * @param {ProgressEvent} e The XHR ProgressEvent.
   * @private
   */
  showCommiters: function () {
	$.getJSON(this.searchOnBitBucket_ , function (json) {
	  var hashes = [];
	  var authors = [];
	  $.each(json.changesets, function (index, result) {
          document.write(result.node + " ");
		  document.write(result.author);
		  document.write("<br/>");
		  hashes.push(result.node);
		  authors.push(result.author);
      });
	  console.log(hashes.join(", "));
	  console.log(authors.join(", "));
	})
  }
   
};

document.addEventListener('DOMContentLoaded', function () {
  gitKataBestCommiter.showCommiters();
});
